import XCTest

import RoadToSwiftTests

var tests = [XCTestCaseEntry]()
tests += RoadToSwiftTests.allTests()
XCTMain(tests)
