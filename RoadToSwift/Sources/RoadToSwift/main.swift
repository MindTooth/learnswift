// print("Hello, world!")

// Variables

// var hello = "Hello, world!"
//
// print(hello)
//
// let length = 45
// let height = 20
//
// var area: Int { return length * height }
//
// print(area)
//
// struct Car {
//     let make: String
//     let year: Int
//
//     func display() {
//         print("Make: \(make)\nYear: \(year)")
//     }
// }
//
// print()
//
// let volvo = Car(make: "Volvo", year: 1993)
// volvo.display()
//
// switch area {
// case 1 ..< 40:
//     print("Yay!")
// case 40 ..< 1000:
//     print("Nay!")
// default:
//     print("Neither...")
// }

func hello() { print("Hello, world!") }

typealias MenuItem = (String, String, String, ())

let kundeMenu: [MenuItem] = [
    ("K", "N", "Legg til ny kunde", hello()),
]

print("Kalles jeg her")

let menu = """

    Hello world!

    Press A for  awseomess

    H - HSest
    B - SKjdslkd
    Q - Quit

"""

func run() {
    print(menu)

    for kunde in kundeMenu {
        print(kunde.0)
        print(kunde.3)
    }
    // print(KUNDE_MENU)

    func inputFunction() -> String? {
        print("Enter choice: ", terminator: "")
        return readLine()!
    }

    while let input = inputFunction() {
        if input.uppercased() == "Q" {
            return
        }

        switch input.uppercased() {
        case "A":
            print("Choice A!\n")
        case "M":
            print(menu)
        default:
            print("Please enter correct choice!\n")
            continue
        }
    }
}

run()
